# Permutation files for Column Reordering

## What does this folder contain?

Herein you’ll find the column permutations used in Table 5 of our article.

---

## How to generate this column-permutation files?

For the code needed to generate this files, please navigate up into the `reordering` folder.
