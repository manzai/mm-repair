# Permutation files for Column Reordering

## What does this folder contain?

Herein you can find suitable column permutations which can be used to reorder columns as for achieving better compression.

---

## How to generate this column-permutation files?

For the code needed to generate this files, please navigate up into the `reordering` folder.
